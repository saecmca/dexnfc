package com.honeywell.aidc.BleDexNfcSample;

import android.text.TextUtils;
import android.util.Log;

public class EDIWriter {
    String segment = "*";
    String newline = "\n";
    StringBuilder EDIFileString = new StringBuilder();

    public void writeApplicationHeader(String CommIDBBU, String functionalIdentifier, String Version, String TCN, String CommIDCustomer, String TestIndicator) {
        String applicationHeader = null;
        if (TextUtils.isEmpty(CommIDBBU))
            Log.e("IVY EDI error DXS", "CommID must");
        else if (TextUtils.isEmpty(functionalIdentifier))
            Log.e("IVY EDI error DXS", "Functional Identifier must");
        else if (TextUtils.isEmpty(Version))
            Log.e("IVY EDI error DXS", "DEX Version must");
        else if (TextUtils.isEmpty(TCN))
            Log.e("IVY EDI error DXS", "Trnasaction control number must");
        else
            applicationHeader = "DXS" + segment + CommIDBBU + segment + functionalIdentifier + segment + Version + segment + TCN + segment + CommIDCustomer + segment + TestIndicator + newline;
        // ediSegments.AppliactionHeader = applicationHeader + newline;
        if (applicationHeader != null)
            EDIFileString.append(applicationHeader);
    }

    public void writeApplicationFooter(String TCN, String transactionSetCount) {
        String applicationFooter = null;
        if (TextUtils.isEmpty(TCN))
            Log.e("IVY EDI error DXE", "Transaction control number must");
        else if (TextUtils.isEmpty(transactionSetCount))
            Log.e("IVY EDI error DXE", "Transaction set count must");
        else
            applicationFooter = "DXE" + segment + TCN + segment + transactionSetCount + newline;
        //ediSegments.ApplicationFooter = applicationFooter + newline;
        if (applicationFooter != null)
            EDIFileString.append(applicationFooter);
    }

    public void writeTransactionHeader(String TransactionSetIdenfierCode, String TransactionSetControlNumber) {
        String trasactionHeader = null;
        if (TextUtils.isEmpty(TransactionSetIdenfierCode))
            Log.e("IVY EDI error ST", "Transaction Set Idenfier Code must");
        else if (TextUtils.isEmpty(TransactionSetControlNumber))
            Log.e("IVY EDI error ST", "Transaction Set Control Number must");
        else
            trasactionHeader = "ST" + segment + TransactionSetIdenfierCode + segment + TransactionSetControlNumber + newline;
        if (trasactionHeader != null)
            EDIFileString.append(trasactionHeader);
    }

    public void writeTransactionFooter(String numberOfIncludedSegments, String TransactionSetControlNumber) {
        String trasactionFooter = null;
        if (TextUtils.isEmpty(numberOfIncludedSegments))
            Log.e("IVY EDI error SE", "Number of included segment must");
        else if (TextUtils.isEmpty(TransactionSetControlNumber))
            Log.e("IVY EDI error SE", "Transaction Set Control Number must");
        else
            trasactionFooter = "SE" + segment + numberOfIncludedSegments + segment + TransactionSetControlNumber + newline;
        if (trasactionFooter != null)
            EDIFileString.append(trasactionFooter);
    }

    public void writeG82Segment(String CreditDebit, String SupplierandDeliveryReturnNumber, String ReceiverDUNSNumber, String ReceiverLocationNumber, String SupplierDUNSNumber, String SupplierLocationNumber, String DeliveryReturnDate, String ProductOwnershipTransferDate, String PurchaseOrderNumber, String PurchaseOrderDate, String MethodofPayment, String CODMethodofPayment) {
        String g82segment = null;
        if (TextUtils.isEmpty(CreditDebit))
            Log.e("IVY EDI error G82", "Credit/Debit must");
        else if (TextUtils.isEmpty(SupplierandDeliveryReturnNumber))
            Log.e("IVY EDI error G82", "Supplier and Delivery Return Number(Invoice number) must");
        else if (TextUtils.isEmpty(ReceiverLocationNumber))
            Log.e("IVY EDI error G82", "Receiver location number must");
        else if (TextUtils.isEmpty(SupplierDUNSNumber))
            Log.e("IVY EDI error G82", "Supplier location number must");
        else if (TextUtils.isEmpty(DeliveryReturnDate))
            Log.e("IVY EDI error G82", "Delivery return date must");
        else
            g82segment = "G82" + segment + CreditDebit + segment + SupplierandDeliveryReturnNumber + segment + ReceiverDUNSNumber + segment + ReceiverLocationNumber + segment + SupplierDUNSNumber + segment + SupplierLocationNumber + segment + DeliveryReturnDate + segment + ProductOwnershipTransferDate + segment + PurchaseOrderNumber + segment + PurchaseOrderDate + segment + MethodofPayment + segment + CODMethodofPayment + newline;
        if (g82segment != null)
            EDIFileString.append(g82segment);
    }

    public void writeG72Segment(String AllowanceorChargeCode, String MethodofHandlingCode, String AllowanceorChargeNumber, String ExceptionNumber, String AllowanceorChargeRate, String AllowanceorChargeQuantity, String UOMCode, String AllowanceorChargeTotalAmount, String AllowanceorChargePercentage, String DollarBasisforPercent, String OptionNumber) {
        String g72segment = null;
        if (TextUtils.isEmpty(AllowanceorChargeCode))
            Log.e("IVY EDI error G72", "Allowance/ChargeCode must");
        else if (TextUtils.isEmpty(MethodofHandlingCode))
            Log.e("IVY EDI error G72", "Method of HandlingCode must");
        else
            g72segment = "G72" + segment + AllowanceorChargeCode + segment + MethodofHandlingCode + segment + AllowanceorChargeNumber + segment + ExceptionNumber + segment + AllowanceorChargeQuantity + segment + UOMCode + segment + AllowanceorChargeTotalAmount + segment + AllowanceorChargePercentage + segment + DollarBasisforPercent + segment + OptionNumber + newline;
        if (g72segment != null)
            EDIFileString.append(g72segment);
    }

    public void writLoopHeader(String LoopIdentifier) {
        String loopHeader = null;
        if (TextUtils.isEmpty(LoopIdentifier))
            Log.e("IVY EDI error LS", "LoopIdentifier must");
        else loopHeader = "LS" + segment + LoopIdentifier + newline;
        if (loopHeader != null)
            EDIFileString.append(loopHeader);
    }

    public void writLoopFooter(String LoopIdentifier) {
        String loopFooter = null;
        if (TextUtils.isEmpty(LoopIdentifier))
            Log.e("IVY EDI error LE", "LoopIdentifier must");
        else
            loopFooter = "LE" + segment + LoopIdentifier + newline;
        if (loopFooter != null)
            EDIFileString.append(loopFooter);
    }

    public void writeG83Segment(String DSDSequenceNumber, String Quantity, String UOMCode, String UPCRetailSellingUnitCode, String ProductIDQualifier, String ProductID, String UPCCaseCode, String ItemListCost, String Pack, String ItemDescription) {
        String g83segment = null;
        if (TextUtils.isEmpty(DSDSequenceNumber))
            Log.e("IVY EDI error G83", "Sequence number must");
        else if (TextUtils.isEmpty(Quantity))
            Log.e("IVY EDI error G83", "Quanity must");
        else if (TextUtils.isEmpty(UOMCode))
            Log.e("IVY EDI error G83", "UOM(unit of measurement) must");
        else if (TextUtils.isEmpty(UPCRetailSellingUnitCode))
            Log.e("IVY EDI error G83", "Universal Product Code - U.P.C. must");
        else if (TextUtils.isEmpty(ItemListCost))
            Log.e("IVY EDI error G83", "Item cost must");
        else
            g83segment = "G83" + segment + DSDSequenceNumber + segment + Quantity + segment + UOMCode + segment + UPCRetailSellingUnitCode + segment + ProductIDQualifier + segment + ProductID + segment + UPCCaseCode + segment + ItemListCost + segment + Pack + segment + ItemDescription + segment + newline;
        if (g83segment != null)
            EDIFileString.append(g83segment);
    }

    public void writeG84Segment(String Quantity, String TotalInvoiceAmount) {
        String g84segment = null;
        if (TextUtils.isEmpty(TotalInvoiceAmount))
            Log.e("IVY EDI error G84", "Total invoice amount must");
        else
            g84segment = "G84" + segment + Quantity + segment + TotalInvoiceAmount + newline;
        if (g84segment != null)
            EDIFileString.append(g84segment);
    }

    public void writeG85Segment(String IntegrityCheckValue) {
        String g85segment = null;
        if (TextUtils.isEmpty(IntegrityCheckValue))
            Log.e("IVY EDI error G85", "Integrity check value must");
        else
            g85segment = "G85" + segment + IntegrityCheckValue + newline;
        if (g85segment != null)
            EDIFileString.append(g85segment);
    }

    public void writeG86Segment(String Signature, String name) {
        String g86segment = "G86" + segment + Signature +segment+name+ newline;
        if (g86segment != null)
            EDIFileString.append(g86segment);
    }

    public void writeG22Segment(String preprice, String prepriceNew) {
        String g22segment = "G22" + segment + preprice + segment + prepriceNew + newline;
        if (g22segment != null)
            EDIFileString.append(g22segment);
    }

    /*Acknowledgement*/
    public void writeG87Segment(String initCode, String crdDbt, String supplierNo, String interigtyCheck, String adjustNo, String receiverNo) {
        String g22segment = "G87" + segment + initCode + segment + crdDbt +segment+supplierNo+segment+interigtyCheck+segment+adjustNo+segment+receiverNo+ newline;
        if (g22segment != null)
            EDIFileString.append(g22segment);
    }


}
