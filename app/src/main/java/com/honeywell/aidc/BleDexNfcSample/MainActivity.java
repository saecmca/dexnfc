package com.honeywell.aidc.BleDexNfcSample;

import android.Manifest;
import android.app.Activity;
import android.app.AutomaticZenRule;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.honeywell.aidc.BleDexNfcSample.Utils.Constants;
import com.honeywell.aidc.BleDexNfcSample.gozer.DefaultCrc16;
import com.honeywell.aidc.dex.*;

public class MainActivity extends Activity implements
        BleDexNfcTag.NdefReadCallback,
        BleDexDevice.ConnectCallback,
        BleDexDevice.SendDataCallback,
        BleDexDevice.OnDataReceiveListener,
        BleDexDevice.OnDisconnectListener,
        ActivityCompat.OnRequestPermissionsResultCallback {
    private Tag nfcTag;
    private TextView tvNfcData;
    private TextView tvBleStatus;
    private TextView tvmacManualEntry;
    private Button btnConnectToDex;
    private Button btnSendDataToDex;
    private Button btnDisconnectDex;
    private Button btnApplyMac;
    private BleDexDevice mBLEDexDevice;
    private String mBtInfo;
    private final int LOCATION_PERMISSION_CODE = 0;
    private int iteratorPermissionRetry = 1;
    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] nfcIntentFilterArray;
    private BluetoothAdapter mBluetoothAdapter;
    private String macAddrManual;
    private ConditionVariable SendDataCompleteLock = new ConditionVariable();
    private volatile boolean BleWriteFailure = false;
    private String receiveCon = "";
    long ccittCrc, ccittCrc1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //crc calculation
        crc161();


        tvNfcData = (TextView) findViewById(R.id.tvNfcData);
        tvmacManualEntry = (TextView) findViewById(R.id.macManualEntry);
        tvBleStatus = (TextView) findViewById(R.id.tvStatusLog);
        tvBleStatus.setMovementMethod(new ScrollingMovementMethod());
        btnConnectToDex = findViewById(R.id.btnConnectToDex);
        btnSendDataToDex = findViewById(R.id.btnSendDataToDex);
        btnDisconnectDex = findViewById(R.id.btnDisconnectDex);
        btnApplyMac = findViewById(R.id.btn_ApplyMAC);
        btnApplyMac.setEnabled(true);
        btnConnectToDex.setEnabled(false);
        btnSendDataToDex.setEnabled(false);
        btnDisconnectDex.setEnabled(false);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        Intent nfcIntent = new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        nfcPendingIntent = PendingIntent.getActivity(this, 0, nfcIntent, 0);
        IntentFilter nfcIntentFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            nfcIntentFilter.addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            updateBleStatusTextView("Mime type exception: " + e.getMessage());
            Log.e(Constants.LOG_TAG, "Mime type exception: " + Log.getStackTraceString(e));
        }
        nfcIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        nfcIntentFilterArray = new IntentFilter[]{nfcIntentFilter};

        //Check for required permissions
        checkForPermissionGrant(Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    public static int crc16fast_U(int crc_U, byte data_U) {
        int[] crctable_U = {0x0000, 0xCC01, 0xD801, 0x1400, 0xF001, 0x3C00, 0x2800, 0xE401, 0xA001, 0x6C00, 0x7800, 0xB401, 0x5000, 0x9C01, 0x8801, 0x4400};
        crc_U = ((crc_U) ^ (Byte.toUnsignedInt(data_U)) & 0x00FF); // Data & CRC shift right, ie LSB first
        crc_U = ((crc_U) >> 4 ^ (crctable_U[(crc_U) & 0xF])); // Process 8-bits, 2 rounds of 4-bits
        crc_U = ((crc_U) >> 4 ^ (crctable_U[(crc_U) & 0xF])); // Right shifting polynomial
        return crc_U;
    }

    private void crc161() {
        {
            byte[] bytes = "009900001000R01L01".getBytes();// args[0].getBytes();
            int crc = 0x0000;
            for (byte b : bytes) {
                crc = crc16fast_U(crc, b);
            }
            crc = crc16fast_U(crc, (byte) 0x03);
            Log.w("crc  low", "" + (((crc >> 0) & 0x00FF)));
            Log.w("crc  high", "" + ((crc >> 8) & 0x00FF));


        }

    }

    /**
     * Check and prompt user for required permissions.
     *
     * @param permission String: The requested permission.
     */
    private void checkForPermissionGrant(String permission) {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(this, permission)) {
            ActivityCompat.requestPermissions(this, new String[]{permission}, LOCATION_PERMISSION_CODE);
        } else {
            checkForNFCEnable();
            checkForBTEnable();
        }
    }

    /**
     * This callback for receiving the results for permission requests.
     *
     * @param requestCode  int: The request code passed in requestPermissions(android.app.Activity, String[], int).
     * @param permissions  String: The requested permissions. Never null.
     * @param grantResults int: The grant results for the corresponding permissions which is either PERMISSION_GRANTED or PERMISSION_DENIED. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    updateBleStatusTextView("Permission grant success.");
                    Toast.makeText(this, "Permission grant success.", Toast.LENGTH_LONG).show();
                    checkForNFCEnable();
                    checkForBTEnable();
                } else {
                    //Request grant for for 2 more times.
                    if (iteratorPermissionRetry <= 2) {
                        iteratorPermissionRetry++;
                        checkForPermissionGrant(Manifest.permission.ACCESS_COARSE_LOCATION);
                    }
                }
            }
        }
    }

    /**
     * Determine whether the device support NFC.
     * If supported and disabled, prompt the user to enable NFC.
     */
    private void checkForNFCEnable() {
        // Use this check to determine whether NFC is supported on the device.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
            updateBleStatusTextView(getString(R.string.nfc_not_supported));
            Toast.makeText(this, R.string.nfc_not_supported, Toast.LENGTH_LONG).show();
        } else {
            //Prompt user to enable NFC
            if (false == nfcAdapter.isEnabled()) {
                updateBleStatusTextView(getString(R.string.turn_on_nfc));
                Toast.makeText(this, R.string.turn_on_nfc, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Determine whether the device support BLE.
     * If supported and disabled, prompt the user to enable BT.
     */
    private void checkForBTEnable() {
        // Use this check to determine whether BLE is supported on the device.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            updateBleStatusTextView(getString(R.string.ble_not_supported));
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_LONG).show();
        } else {
            //Programmatically enable BT
            if (false == mBluetoothAdapter.isEnabled()) {
                updateBleStatusTextView(getString(R.string.turn_on_bt));
                Toast.makeText(this, R.string.turn_on_bt, Toast.LENGTH_LONG).show();
                mBluetoothAdapter.enable();
            } else {
                if (nfcAdapter.isEnabled()) {
                    updateBleStatusTextView("1. Tap DEX Adapter to mobile device or 2. Enter MAC address and click \"Apply MAC\" button.");
                }
            }
        }
    }

    /**
     * BroadcastReceiver to monitor the BT state post mBluetoothAdapter.enable().
     */
    private final BroadcastReceiver mBroadcastReceiverBT = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                switch (state) {
                    case BluetoothAdapter.STATE_ON:
                        //Indicates the local Bluetooth adapter is on, and ready for use.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnApplyMac.setEnabled(true);
                                btnConnectToDex.setEnabled(false);
                                btnSendDataToDex.setEnabled(false);
                                btnDisconnectDex.setEnabled(false);
                                //Indicates the local Bluetooth adapter is on, and ready for use.
                                updateBleStatusTextView(getString(R.string.turn_on_bt_success));
                                if (nfcAdapter.isEnabled()) {
                                    updateBleStatusTextView("1. Tap DEX Adapter to mobile device or 2. Enter MAC address and click \"Apply MAC\" button.");
                                }
                            }
                        });
                        Toast.makeText(context, R.string.turn_on_bt_success, Toast.LENGTH_LONG).show();
                        break;
                    case BluetoothAdapter.STATE_OFF:
                        //Indicates the local Bluetooth adapter is OFF
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnApplyMac.setEnabled(false);
                                btnConnectToDex.setEnabled(false);
                                btnSendDataToDex.setEnabled(false);
                                btnDisconnectDex.setEnabled(false);
                                updateBleStatusTextView(getString(R.string.turn_off_bt));
                            }
                        });
                        break;
                }
            }
        }
    };

    /**
     * BroadcastReceiver to monitor the NFC state post manual enable by user.
     */
    private final BroadcastReceiver mBroadcastReceiverNFC = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (NfcAdapter.ACTION_ADAPTER_STATE_CHANGED.equals(action)) {
                int state = intent.getIntExtra(NfcAdapter.EXTRA_ADAPTER_STATE, -1);
                switch (state) {
                    case NfcAdapter.STATE_ON:
                        //Indicates the NFC is on, and ready for use.
                        updateBleStatusTextView("NFC turned ON successfully!");
                        updateBleStatusTextView("1. Tap DEX Adapter to mobile device or 2. Enter MAC address and click \"Apply MAC\" button.");
                        break;
                }
            }
        }
    };

    /**
     * OnClick handler for the button BtnApplyMac
     *
     * @param view
     */
    public void onClickBtnApplyMac(View view) {
        try {
            macAddrManual = tvmacManualEntry.getText().toString();
            mBLEDexDevice = new BleDexDevice(getApplicationContext(), macAddrManual);
            if (null != mBLEDexDevice) {
                mBLEDexDevice.setDataReceiveListener(this);
                mBLEDexDevice.setDisconnectListener(this);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if ((mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON))
                            btnConnectToDex.setEnabled(true);
                        else {
                            btnConnectToDex.setEnabled(false);
                            updateBleStatusTextView("BT turned OFF. Turn ON BT.");
                        }
                        btnSendDataToDex.setEnabled(false);
                        btnDisconnectDex.setEnabled(false);
                        updateBleStatusTextView("Apply MAC success.");
                    }
                });
            }
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btnConnectToDex.setEnabled(false);
                }
            });
            updateBleStatusTextView("BleDexDevice initialization failed. Exception: " + e.getMessage());
            Log.e(Constants.LOG_TAG, "BleDexDevice initialization failed. Exception: " + Log.getStackTraceString(e));
        }
    }

    /**
     * OnClick handler for the button BtnConnectToDex
     *
     * @param view
     */
    public void onClickBtnConnectToDex(View view) {
        if ((null != mBLEDexDevice)) {
            try {
                btnApplyMac.setEnabled(false);
                btnConnectToDex.setEnabled(false);
                btnSendDataToDex.setEnabled(false);
                btnDisconnectDex.setEnabled(false);
                updateBleStatusTextView("Connecting to DEX.");
                mBLEDexDevice.connect(MainActivity.this);
            } catch (Exception e) {
                updateBleStatusTextView("BLE DEX connect failed with exception: " + e.getMessage());
                Log.e(Constants.LOG_TAG, "BLE DEX connect failed with exception: " + Log.getStackTraceString(e));
            }
        }
    }

    /**
     * OnClick handler for the button BtnSendDataToDex
     *
     * @param view
     */
    public void onClickBtnSendDataToDex(View view) {
        try {
            if (null != mBLEDexDevice) {
                convertToBytes();
                final byte[] byteDataToDex = {'\u0005'};
                // SendDataCompleteLock.close();

                sendControl(byteDataToDex);


                //  mBLEDexDevice.sendData(byteDataToDex, MainActivity.this, 5000);


               /* if (! SendDataCompleteLock.block( 7500 )) {
                    BleWriteFailed = true;
                } else if (BleWriteFailure) {
                    BleWriteFailed = true;
                }
                for (byte b : byteDataToDex) {
                    byte[] s = {b};
                    mBLEDexDevice.sendData(s, MainActivity.this);
                }*/


            }
        } catch (Exception e) {
            updateBleStatusTextView("BLE DEX data send failed with exception: " + e.getMessage());
            Log.e(Constants.LOG_TAG, "BLE DEX data send failed with exception: " + Log.getStackTraceString(e));
        }
    }

    private void sendControl(final byte[] byteDataToDex) {

        Log.w("Send ENQ", "steps 1");
        for (int i = 1; i <= 1; ) {
            i = i + 1;
            sleepThreadForTime(1500);
            //  mBLEDexDevice.sendData(byteDataToDex, MainActivity.this);
            if (receiveCon.equals("\u0005") || receiveCon.equals("\u00100") || receiveCon.equals("\u00100\u00100") || receiveCon.equals("\u00100\u00100\u00100") || receiveCon.equals("\u00100\u00100\u00100\u00100\u00100\u00100")) {
                break;
            } else {
                mBLEDexDevice.sendData(byteDataToDex, MainActivity.this);
            }
        }


        /*byte[] byteDataToDex1 = {'\u0010', '\u0001'};
        mBLEDexDevice.sendData(byteDataToDex1, MainActivity.this);
        mBLEDexDevice.sendData(bytes, MainActivity.this);*/

    }

    private void afterFirstEnq() {
        if (receiveCon.equals("\u00100") || receiveCon.equals("\u00100\u00100") || receiveCon.equals("\u00100\u00100\u00100") || receiveCon.equals("\u00100\u00100\u00100\u00100\u00100\u00100")) {
            // receiveCon="";
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    byte[] byteDataToDex1 = {'\u0010'};
                    mBLEDexDevice.sendData(byteDataToDex1, MainActivity.this);
                }
            }, 1500);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    byte[] byteDataToDex1 = {'\u0001'};
                    mBLEDexDevice.sendData(byteDataToDex1, MainActivity.this);
                }
            }, 1500);
           /* byte[] byteDataToDex1 = {'\u0010', '\u0001'};
            for (byte b : byteDataToDex1) {
                byte[] s = {b};
                mBLEDexDevice.sendData(s, MainActivity.this);
                Log.w("Send DLE SOH", "steps 2: received=" + receiveCon + "=send" + new String(s));
            }*/
            // mBLEDexDevice.sendData(byteDataToDex1, MainActivity.this);

            //Changer is master
            // byte[] byteDataToDex2 = {'0', '0', '9', '9', '0', '0', '0', '0', '1', '0', '0', '0', 'R', '0', '1', 'L', '0', '1'};

            //changer is slave
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    byte[] byteDataToDex2 = {'0', '0', '9', '9', '0', '0', '0', '0', '1', '0', '0', '0', 'R', '0', '1', 'L', '0', '1'};
                    for (byte b : byteDataToDex2) {
                        byte[] s = {b};
                        mBLEDexDevice.sendData(s, MainActivity.this);
                        Log.w("Send comm id", "steps 3:" + new String(s));
                    }
                }
            }, 1500);

            //DLE
            final byte[] byteDataToDex3 = {'\u0010'};//('\u001A'=, '\u0015'=)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (byte b : byteDataToDex3) {
                        byte[] s = {b};
                        mBLEDexDevice.sendData(s, MainActivity.this);

                    }
                }
            }, 1500);
            //ETX
            final byte[] byteDataToDex4 = {'\u0003'};//('\u001A'=, '\u0015'=)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (byte b : byteDataToDex4) {
                        byte[] s = {b};
                        mBLEDexDevice.sendData(s, MainActivity.this);

                    }
                }
            }, 1500);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBLEDexDevice.sendData("\uFFFD".getBytes(), MainActivity.this);
                }
            }, 1500);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBLEDexDevice.sendData("\u0069".getBytes(), MainActivity.this);
                    // mBLEDexDevice.sendData("i".getBytes(), MainActivity.this);
                }
            }, 1500);



           /* new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // mBLEDexDevice.sendData("\u0140".getBytes(), MainActivity.this);
                }
            }, 7500);*/
            //CRC


                    /*final byte[] byteDataToDex4 = {'\u0004'};
                    mBLEDexDevice.sendData(byteDataToDex4, MainActivity.this);*/


            //  Log.w("SendDLE CRC", "steps 5:" + "69b5".getBytes());
            // mBLEDexDevice.sendData("\u0004".getBytes(), MainActivity.this);
        }
    }

    public String dumpString(String text) {
        String txt = "";
        for (int i = 0; i < text.length(); i++) {
            txt = "U+" + Integer.toString(text.charAt(i), 16)
                    + " " + text.charAt(i);
            System.out.println("U+" + Integer.toString(text.charAt(i), 16)
                    + " " + text.charAt(i));
        }
        return txt;
    }

    /**
     * OnClick handler for the button BtnDisconnectDex
     *
     * @param view
     */
    protected void sleepThreadForTime(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (Exception ex) {
        }
    }

    public void onClickBtnDisconnectDex(View view) {
        try {
            if (null != mBLEDexDevice) {
                mBLEDexDevice.disconnect();
            }
        } catch (Exception e) {
            updateBleStatusTextView("BLE DEX disconnect failed with exception: " + e.getMessage());
            Log.e(Constants.LOG_TAG, "BLE DEX disconnect failed with exception: " + Log.getStackTraceString(e));
        }
    }

    /**
     * @param intent Intent: Intent to be handled.
     */
    @Override
    protected void onNewIntent(Intent intent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            nfcTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            BleDexNfcTag mBleDexNfcTag = new BleDexNfcTag(nfcTag);
            mBleDexNfcTag.getTagData(this);
        }
    }

    /**
     * This method is invoked to deliver the successful result of the
     * BleDexNfcTag.getTagData method call. It is one of interface methods
     * of BleDexNfcTag.NdefReadCallback interface.
     *
     * @param adapterInfo BleDexNfcTag.BleDexAdapterInfo: Bluetooth info from the NFC tag.
     */
    @Override
    public void onNdefReadResult(BleDexNfcTag.BleDexAdapterInfo adapterInfo) {
        mBtInfo = adapterInfo.getBluetoothInfo();
        try {
            if (null != mBtInfo) {
                mBLEDexDevice = new BleDexDevice(getApplicationContext(), mBtInfo);
                Log.i(Constants.LOG_TAG, "NFC read success. Tag data: " + mBtInfo);
                if (null != mBLEDexDevice) {
                    mBLEDexDevice.setDataReceiveListener(this);
                    mBLEDexDevice.setDisconnectListener(this);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if ((mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON))
                                btnConnectToDex.setEnabled(true);
                            else {
                                btnConnectToDex.setEnabled(false);
                                updateBleStatusTextView("BT turned OFF. Turn ON BT.");
                            }
                            btnSendDataToDex.setEnabled(false);
                            btnDisconnectDex.setEnabled(false);
                            updateBleStatusTextView("NFC read success.");
                            tvNfcData.setText(mBtInfo);
                        }
                    });
                }
            }
        } catch (BleDexException e) {
            updateBleStatusTextView("BleDexDevice creation failed with exception: " + e.getMessage());
            Log.e(Constants.LOG_TAG, "BleDexDevice creation failed with exception: " + Log.getStackTraceString(e));
        }
    }

    /**
     * This method is invoked to deliver the failure status of the
     * BleDexNfcTag.getTagData method call. It is one of interface methods
     * of BleDexNfcTag.NdefReadCallback interface.
     *
     * @param e An Exception object that indicates the failure.
     */
    @Override
    public void onNdefReadFailed(final Exception e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnConnectToDex.setEnabled(false);
                btnSendDataToDex.setEnabled(false);
                btnDisconnectDex.setEnabled(false);
                updateBleStatusTextView("NFC read failed with exception: " + e.getMessage());
            }
        });
        Log.e(Constants.LOG_TAG, "NFC read failed with exception: " + Log.getStackTraceString(e));
    }

    /**
     * Interface definition for a callback to be invoked when the BT connection success.
     */
    @Override
    public void onConnectSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnApplyMac.setEnabled(false);
                btnConnectToDex.setEnabled(false);
                btnSendDataToDex.setEnabled(true);
                btnDisconnectDex.setEnabled(true);
                updateBleStatusTextView("BLE DEX connection success.");
            }
        });
        Log.i(Constants.LOG_TAG, "BLE DEX connection success.");
    }

    /**
     * Interface definition for a callback to be invoked when the BT connection failed.
     *
     * @param e A {@link BleDexException} object containing the error information.
     */
    @Override
    public void onConnectFailed(final BleDexException e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnApplyMac.setEnabled(true);
                btnConnectToDex.setEnabled(true);
                btnSendDataToDex.setEnabled(false);
                btnDisconnectDex.setEnabled(false);
                updateBleStatusTextView("BLE DEX connection failed with exception : " + e.getMessage());
            }
        });
        Log.e(Constants.LOG_TAG, "BLE DEX connection failed with exception : " + Log.getStackTraceString(e));
    }

    /**
     * Interface definition for a callback to be invoked when the data sent to DEX success.
     *
     * @param data byte: Data to DEX
     */
    @Override
    public void onSendDataSuccess(byte[] data) {
        BleWriteFailure = false;
        SendDataCompleteLock.open();
        final String sentData = new String(data);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateBleStatusTextView("BLE DEX data send success. Data: " + "\"" + sentData + "\"");
            }
        });
        Log.i(Constants.LOG_TAG, "BLE DEX data send success. Data: " + sentData);
    }

    /**
     * Interface definition for a callback to be invoked when the data sent to DEX failed.
     *
     * @param dataFailedToSend A byte array containing the data that failed to send.
     * @param e                A {@link BleDexException} object containing the error information.
     */
    @Override
    public void onSendDataFailed(byte[] dataFailedToSend, final BleDexException e) {
        BleWriteFailure = true;
        SendDataCompleteLock.open();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateBleStatusTextView("BLE DEX data send fail with exception: " + e.getMessage());
            }
        });
        Log.e(Constants.LOG_TAG, "BLE DEX data send fail with exception: " + Log.getStackTraceString(e));
    }

    /**
     * Interface definition for a callback to be invoked when the data received from DEX success.
     *
     * @param data Data from DEX.
     */
    @Override
    public void onDataReceive(final byte[] data) {
        final String receivedData = new String(data);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                receiveControl(receivedData);
                Log.i(Constants.LOG_TAG, "BLE DEX data receive success. Data: " + receivedData);
                if (receivedData.equals("\u0005")) {
                    byte[] byteDataToDex1 = {'\u0010', '0'};
                    mBLEDexDevice.sendData(byteDataToDex1, MainActivity.this);
                }
                if (receivedData.contains("L01\u0010\u0003")) {
                    byte[] byteDataToDex1 = {'\u0010'};
                    mBLEDexDevice.sendData(byteDataToDex1, MainActivity.this);
                    mBLEDexDevice.sendData("1".getBytes(), MainActivity.this);
                }
                if (receivedData.contains("\u0004")) {
                    byte[] byteDataToDex1 = {'\u0005'};
                    mBLEDexDevice.sendData(byteDataToDex1, MainActivity.this);
                }

                if (receiveCon.equals("\u00100") || receiveCon.equals("\u00100\u00100") || receiveCon.equals("\u00100\u00100\u00100") || receiveCon.equals("\u00100\u00100\u00100\u00100\u00100\u00100")) {
                    afterFirstEnq();
                }
                /*byte[] byteDataToDex1 = {'\u0010', '\u0001'};
                for (byte b : byteDataToDex1) {
                    byte[] s = {b};
                    mBLEDexDevice.sendData(s, MainActivity.this);
                    Log.w("Send DLE SOH", "steps 2: received=" + receiveCon + "=send" + new String(s));
                }
                // mBLEDexDevice.sendData(byteDataToDex1, MainActivity.this);
                byte[] byteDataToDex2 = {'0', '0', '9', '9', '0', '0', '1', '0', '0','0','0','0','R','0','1','L','0','1'};
                for (byte b : byteDataToDex2) {
                    byte[] s = {b};
                    mBLEDexDevice.sendData(s, MainActivity.this);
                    Log.w("Send comm id", "steps 3:" + new String(s));
                }
                //DLE ETX CRC
                byte[] byteDataToDex3 = {'\u0010','\u0003','\u0013'};
                mBLEDexDevice.sendData(byteDataToDex3, MainActivity.this);*/
                /*for (byte b : byteDataToDex3) {
                    byte[] s = {b};
                    mBLEDexDevice.sendData(s, MainActivity.this);
                    Log.w("SendDLE ETX CRC", "steps 4:" + new String(s));
                }
                receiveControl(receivedData);*/

                /*//DLE SOH
                byte[] byteDataToDex1 = {'\u0010', '\u0001'};
                for (byte b : byteDataToDex1) {
                    byte[] s = {b};
                    mBLEDexDevice.sendData(s, MainActivity.this);
                }

                //EDI
                 for (byte b : bytes) {
                    byte[] s = {b};
                    mBLEDexDevice.sendData(s, MainActivity.this);
                }*/
                // mBLEDexDevice.sendData(bytes, MainActivity.this);
                updateBleStatusTextView("BLE DEX data receive success. Data: " + "\"" + receivedData + "\"");
                //   receiveControl(receivedData);
                /*if(data.equals(" \u0010")){
                    for (byte b : bytes) {
                        byte[] s = {b};
                        mBLEDexDevice.sendData(s, MainActivity.this);
                    }
                }*/

            }
        });

    }

    private void receiveControl(String data) {
        receiveCon = new String(data);
    }


    /**
     * Disconnect from DEX.
     */
    @Override
    public void onDisconnect() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateBleStatusTextView("BLE DEX disconnected.");
                if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
                    btnApplyMac.setEnabled(true);
                    btnConnectToDex.setEnabled(true);
                } else {
                    btnApplyMac.setEnabled(false);
                    btnConnectToDex.setEnabled(false);
                    updateBleStatusTextView("BT turned OFF. Turn ON BT.");
                }
                btnSendDataToDex.setEnabled(false);
                btnDisconnectDex.setEnabled(false);
            }
        });
        Log.i(Constants.LOG_TAG, "BLE DEX disconnected.");
    }

    /**
     * Update the status text view.
     *
     * @param logMessage String: status message.
     */
    private void updateBleStatusTextView(String logMessage) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss (MM/dd/yyyy)");
        String currentDateandTime = simpleDateFormat.format(new Date());
        tvBleStatus.setText("\n" + currentDateandTime + " - " + logMessage + tvBleStatus.getText());
    }

    @Override
    public void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mBroadcastReceiverBT, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        registerReceiver(mBroadcastReceiverNFC, new IntentFilter(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED));
        nfcAdapter.enableForegroundDispatch(
                this,
                nfcPendingIntent,
                nfcIntentFilterArray,
                null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiverBT);
        unregisterReceiver(mBroadcastReceiverNFC);
    }

    private byte[] bytes;

    void convertToBytes() {
        try {
            InputStream inputStream = getResources().openRawResource(R.raw.file_send);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = inputStream.read(data, 0, data.length)) != -1)
                buffer.write(data, 0, nRead);

            bytes = buffer.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}